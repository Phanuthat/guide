import React, { Component } from "react";
import {
  Card,
  Comment,
  Avatar,
  Form,
  Button,
  Input,
  message,
  Rate,
  notification
} from "antd";
import { connect } from "react-redux";
import axios from "axios";
const TextArea = Input.TextArea;
const desc = [1, 2, 3, 4, 5];

class Comments extends Component {
  state = {
    comment: [],
    id: "",
    token: "",
    star: 0
  };
  componentWillMount() {
    console.log("comment=========================>", this.props);
    console.log("comment id", this.props.id);
    console.log("comment dispatch", this.props.dispatch);
    this.setState({ id: this.props.id, token: this.props.token });
  }
  callReviewAll = () => {
    const { id } = this.state;
    axios
      .get(`http://3.88.222.254:8888/reviews/shop/${id}`)
      .then(review => {
        console.log(
          "review ====================================================>",
          review.data
        );

        this.props.OnLoadReview(review.data);
        this.forceUpdate();
      })
      .catch(error => {
        console.log("error" + error);
      });
  };

  handleChange = comment => {
    const { token, id, comments, star } = this.state;
    const { user } = this.props.UserInfo;
    console.log("token comment page ====================>", token);
    console.log("user comment page ====================>", user);
    if (token == null) {
      message.warning("Please Login");
    } else if (star === 0) {
      message.warning("Please rate your satisfaction.");
    } else {
      axios({
        url: `http://3.88.222.254:8888/reviews/${user.id}`,
        method: "post",
        headers: { Authorization: `Bearer ${token}` },
        data: {
          review: comments,
          shopID: id,
          start: star
        }
      })
        .then(res => {
          console.log("res=========================== res", res);

          message.success("Comment Success", 1, () => {
            this.callReviewAll();
          });
        })
        // .then(() => {
        //   window.location.reload();
        // })
        .catch(error => {
          console.log(error);
        });
    }
  };

  commentChange = event => {
    const comments = event.target.value;

    this.setState({ comments });
  };

  starChange = event => {
    const star = event;

    this.setState({ star });
  };
  render() {
    return (
      <div style={{ paddingLeft: "50px", paddingRight: "50px" }}>
        <Card style={{ width: "100%" }}>
          <Comment
            avatar={
              <Avatar
                src={this.props.UserInfo.image ==null? "https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png":this.props.UserInfo.image}
                // alt={email}
              />
            }
            content={
              <div>
                <Form.Item>
                  <TextArea rows={4} onChange={this.commentChange} />
                </Form.Item>
                <Form.Item>
                  <div style={{ textAlign: "left", alignItems: "left" }}>
                    <p>
                      ความพอใจ :{" "}
                      <Rate tooltips={desc} onChange={this.starChange} />
                    </p>
                  </div>
                </Form.Item>
                <Form.Item>
                  <Button
                    htmlType="submit"
                    // loading={submitting}
                    onClick={() => {
                      this.handleChange(this.state.comments);
                    }}
                    type="primary"
                  >
                    Add Comment
                  </Button>
                </Form.Item>
              </div>
            }
          />
        </Card>
      </div>
    );
  }
}
const mapStateToProps = state => {
  console.log("mapStateToProps comment", state);
  return {
    Category: state.Category,

    UserInfo: state.UserInfo
  };
};
const mapDispatchToProps = dispatch => {
  return {
    OnLoadReview: item =>
      dispatch({
        type: "LOAD_REVIEW",
        loadReview: item
      })
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Comments);
