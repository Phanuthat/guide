import React, { Component } from "react";
import {
  Map,
  InfoWindow,
  Marker,
  GoogleApiWrapper,
  Listing,
  MarkerImage
} from "google-maps-react";
import fabric from "../../Picture/fabric.png";
import { withRouter } from "react-router-dom";
class MapByShop extends Component {
  componentWillMount() {
    const { id } = this.props.location.state;

    console.log("this.props map shop ======>", this.props.location.state);
  }
  render() {
    console.log(" state map by shop =========================>", this.state);
    console.log("this.props.google ===================>", this.props.google);
    const { latitude, longitude } = this.props.location.state.location;
    const { name } = this.props.location.state.lang.th;
    return (
      <div style={{ paddingTop: "100px", paddingLeft: "30px" }}>
        <h2>แผนที่ ร้าน {name}</h2>
        <Map
          center={{
            lat: 18.8043949,
            lng: 98.9526236
          }}
          onClick={this.onMapClicked}
          google={this.props.google}
          zoom={14}
          style={{ width: "80%", height: "80%" }}
        >
          <Marker
            onClick={this.onMarkerClick}
            // name={makers}
            position={{
              lat: latitude,
              lng: longitude
            }}
          />
        </Map>
      </div>
    );
  }
}
export default withRouter(
  GoogleApiWrapper({
    apiKey: "AIzaSyA84pogLvst4NROG-DwN5NaHCkRX7Yk9bM"
  })(MapByShop)
);
