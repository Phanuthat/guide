import React, { Component } from "react";
import axios from "axios";
import { List, Avatar, Icon, message, Tag, Typography } from "antd";
import { connect } from "react-redux";
import { Spin } from "antd";
import { element } from "prop-types";
const { Title, Paragraph, Text } = Typography;
var _ = require("lodash");

const star = () => (
  <div>
    <p>ss</p>
  </div>
);

class ShopList extends Component {
  state = {
    shopList: [],
    star: [],
    shopID: [],
    avg: 0,
    rateStar: []
  };

  averrage = res => {
    let rateStar = _.map(this.props.ShopList.data, value => {
      const data = _.filter(res.data, { shopID: value.id });
      const sum = _.sumBy(data, value => {
        return value.start;
      });
      return { ...value, sumReview: sum / data.length };
    });
    this.setState({ shopList: rateStar });
    console.log("rateStar =========================>", rateStar);
  };

  componentWillMount() {
    axios
      .get("https://chiangmai.thaimarket.guide/shop?offset=0&limit=100")
      .then(res => {
        this.props.onLoader(res.data.data);
      })

      .catch(error => {
        console.log("error" + error);
      });

    axios
      .get("http://3.88.222.254:8888/reviews/all")
      .then(res => {
        this.averrage(res);
      })
      .catch(error => {
        console.log("error", error);
      });
  }
  onClickShop = id => {
    this.props.history.push("/shop", { id, user: this.props.UserInfo });
  };

  render() {
    let shopListCategory = this.state.shopList;
    const { category } = this.props.Category;
    if (category !== "ทั้งหมด") {
      shopListCategory = this.state.shopList.filter(e => {
        return this.props.Category.category === e.category;
      });
    }

    console.log("shopListCategory ====================>", shopListCategory);
    return (
      <div style={{ padding: "50px", paddingTop: "100px" }}>
        <List
          itemLayout="vertical"
          size="large"
          pagination={{
            onChange: page => {
              console.log(page);
              message.success("Page" + page);
            },
            pageSize: 5
          }}
          dataSource={shopListCategory}
          renderItem={item => (
            <List.Item
              onClick={() => this.onClickShop(item.id)}
              key={item.title}
              extra={
                <img
                  width={272}
                  alt="logo"
                  style={{
                    border: "2px solid #D0B3E1",
                    borderRightColor: "#D0B3E1",
                    borderRadius: "20px"
                  }}
                  src={item.image}
                />
              }
            >
              <div />

              <List.Item.Meta
                avatar={
                  <Avatar src="https://image.flaticon.com/icons/svg/891/891448.svg" />
                }
                title={
                  <a>
                    <h2 style={{ fontStyle: "italic", textAlign: "left" }}>
                      {item.lang.th.name}
                    </h2>
                  </a>
                }
                description={
                  <Typography>
                    <Paragraph
                      ellipsis={{ rows: 3, expandable: true }}
                      style={{
                        marginTop: "50px",
                        textAlign: "left",
                        textIndent: "50px",
                        textTransform: "capitalize",
                        textJustify: "inter-word",
                        wordBreak: "break-all"
                      }}
                    >
                      {item.lang.th.description}
                    </Paragraph>
                  </Typography>
                }
              />
              <div
                style={{
                  textAlign: "end",

                  paddingRight: "50px"
                }}
              />
              <p
                style={{
                  textAlign: "end",
                  paddingTop: "120px",
                  paddingRight: "50px"
                }}
              >
                <Tag color="gold">
                  Rate :{" "}
                  {isNaN(item.sumReview) === true
                    ? "ไม่มีคะแนน"
                    : item.sumReview.toFixed(1)}
                </Tag>
                <Tag color="#87d068">
                  {item.status === "ACTIVE"
                    ? "สถานะ : ร้านเปิด"
                    : "สถานะ : ร้านปิด"}
                </Tag>
                <Tag color="#2db7f5">ประเภท : {item.category}</Tag>
              </p>
            </List.Item>
          )}
        />
      </div>
    );
  }
}
const mapStateToProps = state => {
  console.log("mapStateToProps", state);
  return {
    Category: state.Category,
    ShopList: state.ShopList,
    UserInfo: state.UserInfo
  };
};
const mapDispatchToProps = dispatch => {
  return {
    onLoader: item =>
      dispatch({
        type: "SHOP_LIST",
        load: item
      }),
    onDismissDialog: items =>
      dispatch({
        type: "DISMISS_DIALOG"
      })
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ShopList);
