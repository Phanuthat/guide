import React, { Component } from "react";
import { Layout } from "antd";
import { withRouter } from "react-router-dom";
const { Footer } = Layout;
class Footers extends Component {
  render() {
    return (
      <div>
        <Footer
          style={{
              
            textAlign: "center",
            background: "#E0BBE4",
            color: "#FBFCFC",
            fontSize: "18px",
            paddingTop:"10px",
            paddingBottom:"10px"
          }}
        >
          CMU GUIDE
        </Footer>
      </div>
    );
  }
}
export default withRouter(Footers);
