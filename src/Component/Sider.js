import React, { Component } from "react";
import { Menu, Icon, Layout, message } from "antd";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
const { Sider } = Layout;
const { SubMenu } = Menu;
const login = ["login", "logout"];
class SiderBar extends Component {
  state={
    user:{
      id:0
    }
  }
  onChangeCategory = pathName => {
    var path = "/";
    path = `/home`;
    console.log("pathName", pathName);
    this.props.history.replace(path);
    this.props.onClickCategorys(pathName.key);
    console.log("this.props", this.props);
    console.log("this.props location", this.props.location);
  };
  onClickLogout = () => {
    const { token } = this.props.UserInfo;
    console.log(
      "prop =====================================================",
      token
    );
    if (token !== null) {
      this.props.onLogout();
      message.success("Log out sucess", 1, () => {
        window.location.replace("/home")
        // this.props.history.push("/home");
      });
    } else {
      console.log(
        "prop =====================================================",
        token
      );
    }
  };

  OnClick = pathSend => {
    var path = "/";
    path = `/${pathSend.key}`;
    this.props.history.replace(path);
  };

  componentWillMount() {
    console.log("componentWillMount ,sider", this.props.UserInfo.state.length);
    this.setState({user:this.props.UserInfo.user})
  }
OnClickLogin=()=>{
  this.setState({user:this.props.UserInfo.user})
  console.log("OnClickLogin ===========================> ",this.state)
  if(this.state.user === undefined){
    this.props.history.replace("/login");
  }else{
     this.props.history.replace("/home");
  }
}
  render() {
    return (
      <div>
        <Sider
          width={200}
          style={{
            background: "#fff",
            paddingTop: "70px",
            marginBottom: "22px",
            height: "100%"
          }}
        >
          <Menu
            mode="inline"
            // SelectedKeys={[this.state.category]}
            defaultSelectedKeys={["ทั้งหมด"]}
            // defaultOpenKeys={["category"]}
            style={{ height: "100%", borderRight: 0, textAlign: "left" }}
          >
            <Menu.Item
              key="profile"
              onClick={key => {
                this.OnClick(key);
              }}
            >
              <Icon type="user" />
              ข้อมูลส่วนตัว
            </Menu.Item>
            <Menu.Item
              key="home"
              onClick={key => {
                this.OnClick(key);
              }}
            >
              <Icon type="home" />
              หน้าแรก
            </Menu.Item>
            <Menu.Item
              key="map"
              onClick={key => {
                this.OnClick(key);
              }}
            >
              <Icon type="environment" />
              แผนที่
            </Menu.Item>

            <SubMenu
              key="category"
              title={
                <span>
                  <Icon type="shop" />
                  ประเภทร้านค้า
                </span>
              }
            >
              <Menu.Item
                key="ร้านค้า"
                onClick={key => this.onChangeCategory(key)}
              >
                ร้านค้า
              </Menu.Item>
              <Menu.Item
                key="ร้านอาหาร (และเครื่องดื่ม)"
                onClick={key => this.onChangeCategory(key)}
              >
                ร้านอาหาร (และเครื่องดื่ม)
              </Menu.Item>
              <Menu.Item
                key="เครื่องประดับ"
                onClick={key => this.onChangeCategory(key)}
              >
                เครื่องประดับ
              </Menu.Item>
              <Menu.Item
                key="บริการต่างๆ"
                onClick={key => this.onChangeCategory(key)}
              >
                บริการต่างๆ
              </Menu.Item>
              <Menu.Item
                key="คาเฟ่และของหวาน"
                onClick={key => this.onChangeCategory(key)}
              >
                คาเฟ่และของหวาน
              </Menu.Item>
              <Menu.Item
                key="อาหารเอเชีย"
                onClick={key => this.onChangeCategory(key)}
              >
                อาหารเอเชีย
              </Menu.Item>
              <Menu.Item
                key="อาหารไทย"
                onClick={key => this.onChangeCategory(key)}
              >
                อาหารไทย
              </Menu.Item>
              <Menu.Item
                key="สินค้าเบ็ดเตล็ด และอื่นๆ"
                onClick={key => this.onChangeCategory(key)}
              >
                สินค้าเบ็ดเตล็ด และอื่นๆ
              </Menu.Item>
              <Menu.Item
                key="เฟอร์นิเจอร์และของตกแต่งบ้าน"
                onClick={key => this.onChangeCategory(key)}
              >
                เฟอร์นิเจอร์และของตกแต่งบ้าน
              </Menu.Item>
              <Menu.Item
                key="ของที่ระลึก"
                onClick={key => this.onChangeCategory(key)}
              >
                ของที่ระลึก
              </Menu.Item>
              <Menu.Item
                key="ศูนย์การค้า"
                onClick={key => this.onChangeCategory(key)}
              >
                ศูนย์การค้า
              </Menu.Item>
              <Menu.Item
                key="ต้นไม้และอุปกรณ์ทำสวน"
                onClick={key => this.onChangeCategory(key)}
              >
                ต้นไม้และอุปกรณ์ทำสวน
              </Menu.Item>
              <Menu.Item
                key="ของเก่า ของสะสม"
                onClick={key => this.onChangeCategory(key)}
              >
                ของเก่า ของสะสม
              </Menu.Item>
              <Menu.Item
                key="สุขภาพและความงาม"
                onClick={key => this.onChangeCategory(key)}
              >
                สุขภาพและความงาม
              </Menu.Item>
              <Menu.Item key="ตลาด" onClick={key => this.onChangeCategory(key)}>
                ตลาด
              </Menu.Item>
              <Menu.Item
                key="แฟชั่นสตรี"
                onClick={key => this.onChangeCategory(key)}
              >
                แฟชั่นสตรี
              </Menu.Item>
              <Menu.Item
                key="แฟชั่นบุรุษ"
                onClick={key => this.onChangeCategory(key)}
              >
                แฟชั่นบุรุษ
              </Menu.Item>
              <Menu.Item
                key="ทั้งหมด"
                onClick={key => this.onChangeCategory(key)}
              >
                ทั้งหมด
              </Menu.Item>
            </SubMenu>

            <Menu.Item  onClick={path => this.OnClickLogin()}>
              <Icon type="login" />
              ลงชื่อเข้าใช้
            </Menu.Item>
            {this.state.user === undefined? "": <Menu.Item onClick={() => this.onClickLogout()}>
                <Icon type="logout" />
                ออกจากระบบ
              </Menu.Item>
            }
          </Menu>
        </Sider>
      </div>
    );
  }
}
const mapStateToProps = state => {
  return {
    Category: state.Category,
    ShopList: state.ShopList,
    UserInfo: state.UserInfo
  };
};
const mapDispatchToProps = dispatch => {
  return {
    onClickCategorys: item =>
      dispatch({
        type: "CLICK_CATEGORY",
        payload: item
      }),
    onLogout: items =>
      dispatch({
        type: "LOG_OUT",
        logout: items
      })
  };
};
export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(SiderBar)
);
