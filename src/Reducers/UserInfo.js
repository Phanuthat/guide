export default (state = [], action) => {
  switch (action.type) {
    case "ADD_USER":
      return {
        ...state,
        ...action.addData
      };
    case "EDIT_USER":
      return {
        ...state,
        ...action.payload
      };
    case "LOG_OUT":
      return {
        state: []
      };
    default:
      return state;
  }
};
