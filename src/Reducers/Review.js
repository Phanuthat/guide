export default (
  state = {
    review: [
      {
        review: "",
        customer: {
          email: "",
          password: "",

          id: 0,
          firstName: "",
          lastName: "",
          image: ""
        },
        id: 0,
        shopID: "",
        start: 0
      }
    ]
  },
  action
) => {
  switch (action.type) {
    case "LOAD_REVIEW":
      return {
        review: action.loadReview
      };

    default:
      return state;
  }
};
