import React, { Component } from "react";
import {
  Typography,
  Spin,
  Card,
  List,
  Affix,
  Row,
  Col,
  Icon,
  Tag,
  Button
} from "antd";
import ReadComment from "../Component/Shop/Comment";
import Reviews from "../Component/Shop/Review";
import { connect } from "react-redux";
import { Table } from "reactstrap";
import axios from "axios";
import icon from "../Picture/fabric.png";
const { Column, ColumnGroup } = Table;
const { Meta } = Card;
const { Title, Text } = Typography;

var _ = require("lodash");
class Shop extends Component {
  state = {
    shop: {
      category: "",

      id: "",
      image: "",
      lang: { th: {}, en: {} },
      location: { latitude: "", longitude: "" },
      products: [
        {
          lang: { th: {}, en: {} },
          image: ""
        }
      ],
      status: ""
    },
    comments: [],
    submitting: false,
    value: "",
    review: [
      {
        shopID: "",
        review: "",
        id: "",
        customer: ""
      }
    ],
    avg: 0
  };
  OnCLickMap = () => {
    this.props.history.push("/mapbyshop", this.state.shop);
  };

  averrage = res => {
    const { id } = this.props.location.state;

    axios
      .get(`http://3.88.222.254:8888/reviews/shop/${id}`)
      .then(res => {
        const rate = res.data;
        const avg = _.meanBy(rate, value => {
          return value.start;
        });

        this.setState({ avg });
        this.forceUpdate();
      })
      .catch(error => {
        console.log("error" + error);
      });
  };
  componentWillMount() {
    console.log("Shop props =====================>", this.props);
    const { token } = this.props.location.state.user;

    axios
      .get(
        `https://chiangmai.thaimarket.guide/shop/${
          this.props.location.state.id
        }`
      )
      .then(res => {
        this.setState({ shop: res.data.data });
        this.averrage(res.data.data);
      })
      .catch(error => {
        console.log("error" + error);
      });

    console.log("componentWillMount ", this.props);
  }

  componentDidMount() {
    this.forceUpdate();
  }

  render() {
    const { lang, products } = this.state.shop;

    const { id } = this.props.location.state;
    const { token } = this.props.location.state.user;
    console.log("object", this.state.review);

    return (
      <div style={{ paddingTop: "100px" }}>
        <div>
          {/* <Affix
            offsetTop={100}
            style={{
              alignItems: "right",
              backgroundColor: "red",
              color: "red"
            }}
          > */}
            <Button
              color="red"
              size={"large"}
              style={{ position: "absolute", right: "100px" }}
              onClick={() => {
                this.OnCLickMap();
              }}
            >
              <p>
                <img src={icon} width="40px" height="40px" /> Go to map
              </p>
            </Button>
          {/* </Affix> */}
        </div>

        <Row type="flex" justify="start">
          <Col span={6}>
            {" "}
            <Title
              level={2}
              style={{ textAlign: "left", paddingLeft: "60px" }}
              mark
            >
              {lang.th.name}
            </Title>
          </Col>
          <p style={{ fontSize: "20px", paddingRight: "5px" }}>
            <Tag color="#87d068">
              <Icon
                type="star"
                style={{ fontSize: "20px", paddingRight: "5px" }}
              />
              {isNaN(this.state.avg) === true
                ? "ไม่มีคะแนน"
                : this.state.avg.toFixed(1)}
            </Tag>
          </p>
        </Row>

        <div
          style={{
            paddingTop: "30px",
            paddingLeft: "60px",
            justifyItems: "center"
          }}
        >
          <List
            grid={{
              gutter: 16,
              column: 4
            }}
            dataSource={products}
            renderItem={item =>
              item !== null ? (
                <List.Item>
                  <Card
                    hoverable
                    style={{ width: 240 }}
                    cover={<img alt="example" src={item.image} />}
                  >
                    <Meta
                      title={item.lang.th.name}
                      description={item.lang.th.description}
                    />
                  </Card>
                </List.Item>
              ) : (
                <Spin />
              )
            }
          />
        </div>

        <div>
          <Title
            level={3}
            style={{
              paddingTop: "50px",
              textAlign: "left",
              textIndent: "100px"
            }}
          >
            รายละเอียด
          </Title>
        </div>
        <div
          style={{
            paddingTop: "50px",
            textAlign: "left",
            textIndent: "100px",
            textTransform: "capitalize",
            textJustify: "inter-word",
            padding: "50px",
            margin: "auto"
          }}
        >
          <Text>{lang.th.description}</Text>
        </div>
        <div>
          <Title
            level={3}
            style={{
              paddingTop: "50px",
              textAlign: "left",
              textIndent: "100px"
            }}
          >
            รีวิว
          </Title>
          <div style={{ paddingLeft: "50px", paddingRight: "50px" }}>
            <Reviews id={id} />
          </div>
        </div>
        <Title
          level={3}
          style={{
            paddingTop: "50px",
            textAlign: "left",
            textIndent: "100px"
          }}
        >
          เขียนรีวิว
        </Title>
        <ReadComment id={id} token={token} />
      </div>
    );
  }
}
const mapStateToProps = state => {
  console.log("mapStateToProps", state);
  return {
    Category: state.Category,
    ShopList: state.ShopList,
    UserInfo: state.UserInfo
  };
};
export default connect(
  mapStateToProps,
  null
)(Shop);
