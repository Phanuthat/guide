import React, { Component } from "react";
import {
  Map,
  InfoWindow,
  Marker,
  GoogleApiWrapper,
  Listing
} from "google-maps-react";
import axios from "axios";
import TextTruncate from "react-text-truncate";
import { Card, Button } from "antd";
import { withRouter } from "react-router-dom";
const { Meta } = Card;

class MapPage extends Component {
  state = {
    shopList: [],
    showingInfoWindow: false,
    activeMarker: {},
    selectedPlace: {
      id: "",
      name: {
        image: "",
        lang: {
          th: {},
          en: {}
        }
      }
    }
  };

  onClickShop = id => {
    console.log("this.state.selectedPlace.id MapPAge ==================>", this.state.selectedPlace.id);
    this.props.history.push("/shop", { id });
  };
  onMarkerClick = (props, marker, e) =>
    this.setState({
      selectedPlace: props,
      activeMarker: marker,
      showingInfoWindow: true
    });

  onMapClicked = props => {
    if (this.state.showingInfoWindow) {
      this.setState({
        showingInfoWindow: false,
        activeMarker: null
      });
    }
  };

  componentWillMount() {
    axios
      .get("https://chiangmai.thaimarket.guide/shop?offset=0&limit=100")
      .then(res => {
        this.setState({ shopList: res.data.data });
        console.log("selectedPlace", this.state.selectedPlace);
        console.log("shoplist.id", this.state.shopList);
      })

      .catch(error => {
        console.log("error" + error);
      });
  }

  render() {
    const { lang } = this.state.selectedPlace.name;

    return (
      <div style={{ paddingTop: "100px", paddingLeft: "30px" }}>
        <h2>แผนที่ CMU GUIDE</h2>
        <Map
          center={{
            lat: 18.8043949,
            lng: 98.9526236
          }}
          onClick={this.onMapClicked}
          google={this.props.google}
          zoom={14}
          style={{ width: "80%", height: "80%" }}
        >
          {this.state.shopList.map(makers => (
            <Marker
              onClick={this.onMarkerClick}
              name={makers}
              position={{
                lat: makers.location.latitude,
                lng: makers.location.longitude
              }}
            />
          ))}
          <InfoWindow
            marker={this.state.activeMarker}
            visible={this.state.showingInfoWindow}
            onClick={() => {
              this.onClickShop(this.state.selectedPlace.id);
            }}
          >
            <div>
              <Card
                onClick={() => {
                  this.onClickShop();
                }}
                onTabChange={() => {
                  this.onClickShop(this.state.selectedPlace.id);
                }}
                hoverable
                style={{ width: 240 }}
                cover={
                  <img alt="shop" src={this.state.selectedPlace.name.image} />
                }
              >
                <Meta
                  onClick={() => {
                    this.onClickShop(this.state.selectedPlace.id);
                  }}
                  title={lang.th.name}
                  description={
                    <TextTruncate
                      line={2}
                      truncateText="…"
                      text={
                        <p
                          style={{
                            textIndent: "40px",
                            textAlign: "justify",
                            textJustify: "inter-word"
                          }}
                        >
                          {lang.th.description}
                        </p>
                      }
                      textTruncateChild={<a href="#">Read more</a>}
                    />
                  }
                />
                <Button
                  type="primary"
                  loading={this.state.loading}
                  onClick={()=>{this.onClickShop(this.state.selectedPlace.id)}}
                >
                  Click me!
                </Button>
              </Card>
            </div>
          </InfoWindow>
        </Map>
      </div>
    );
  }
}

export default withRouter(
  GoogleApiWrapper({
    apiKey: "AIzaSyA84pogLvst4NROG-DwN5NaHCkRX7Yk9bM"
  })(MapPage)
);
