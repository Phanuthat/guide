import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { Carousel } from "antd";
import {
  Button,
  Avatar,
  Typography,
  Row,
  Col,
  Form,
  Divider,
  Upload,
  message,
  Icon,
  Layout
} from "antd";

import imgCarousel from "../Picture/food.jpg";
import SiderProfile from "../Component/Profile/SiderProfile";
import axios, { post } from "axios";
const { Header, Footer, Sider, Content } = Layout;

const { Title, Paragraph, Text } = Typography;

class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      file: null,
      firstName: "",
      lastName: "User",
      email: "User"
    };
    this.onFormSubmit = this.onFormSubmit.bind(this);
    this.onChange = this.onChange.bind(this);
    this.fileUpload = this.fileUpload.bind(this);
  }
  // state = {
  //   firstName: "User",
  //   lastName: "User",
  //   email: "User@gmail.com",
  //   file: null
  // };
  componentDidMount() {
    console.log(
      "this.prop =======================================",
      this.props.UserInfo.user
    );
    if (this.props.UserInfo.user != null) {
      const { firstName, lastName, email } = this.props.UserInfo.user;
      this.setState({ firstName, lastName, email });
    }
  }
  // this.onFormSubmit = this.onFormSubmit.bind(this)
  //   this.onChange = this.onChange.bind(this)
  //   this.fileUpload = this.fileUpload.bind(this)
  onFormSubmit(e) {
    e.preventDefault(); // Stop form submit
    this.fileUpload(this.state.file).then(response => {
      message.success("success", 1);
      console.log(
        "====================================================",
        response.data
      );
      this.props.EditProfile(response.data);
      console.log(
        "res   ======================== >>>>>>>>>>>>>>>>>>>>>>",
        response
      );
    });
  }
  onChange(e) {
    this.setState({ file: e.target.files[0] });
  }
  fileUpload(file) {
    const { token } = this.props.UserInfo;
    const { id } = this.props.UserInfo.user;
    const url = `http://3.88.222.254:8888/uploadFile/${id}`;
    const formData = new FormData();
    formData.append("file", file);
    const config = {
      headers: {
        "Authorization": `Bearer ${token}`,
        "content-type": "multipart/form-data"
      }
    };
    return post(url, formData, config);
  }
  onClickUpload = file => {
    const { token } = this.props.UserInfo;
    const { id } = this.props.UserInfo.user;
    console.log("id======>", id);
    console.log("file===========================>", file);
 
  };
  render() {
    const { firstName, lastName, email, image } = this.props.UserInfo;

    return (
      <div>
        <Layout>
          <div>
            <Carousel width="100%" height="300px">
              <img src={imgCarousel} width="100%" height="300px" />
            </Carousel>
          </div>

          <Layout>
            <SiderProfile />
            <Layout>
              <div
                style={{
                  paddingTop: "100px"
                }}
              >
                <Avatar
                  size={200}
                  src={image}
                  style={{ color: "#f56a00", backgroundColor: "#fde3cf" }}
                  // icon="user"
                />
              </div>
              <div style={{ paddingTop: "50px" }}>
                {this.props.UserInfo.user === undefined ? (
                  ""
                ) : (
                  <form onSubmit={this.onFormSubmit}>
                    <h1>Upload Image</h1>
                    <input type="file" onChange={this.onChange} />
                    <button type="upload">Upload</button>
                  </form>
                )}

                {/* <Upload>
                  <Button>
                    <Icon type="upload" /> Upload Image
                  </Button>
                </Upload> */}
              </div>

              <Layout style={{ padding: "0 24px 24px" }}>
                <Form style={{ width: "50%", margin: "0 auto" }}>
                  <div
                    className="gutter-example"
                    style={{ paddingTop: "100px" }}
                  >
                    <Row gutter={16}>
                      <Col className="gutter-row" span={8}>
                        <div className="gutter-box">
                          {" "}
                          <Title level={4}>ชื่อ : </Title>
                        </div>
                      </Col>
                      <Col className="gutter-row" span={4}>
                        <div className="gutter-box">{firstName}</div>
                      </Col>
                      <Divider />
                    </Row>
                    <Row>
                      <Col className="gutter-row" span={8}>
                        <div className="gutter-box">
                          <Title level={4}>นามสกุล :</Title>
                        </div>
                      </Col>

                      <Col className="gutter-row" span={4}>
                        <div className="gutter-box">{lastName}</div>
                      </Col>
                      <Divider />
                    </Row>
                    <Row gutter={16}>
                      <Col className="gutter-row" span={8}>
                        <div className="gutter-box">
                          {" "}
                          <Title level={4}>อีเมล์ : </Title>
                        </div>
                      </Col>
                      <Col className="gutter-row" span={4}>
                        <div className="gutter-box">{email == undefined?this.props.UserInfo.user :"5"}</div>
                      </Col>
                      <Divider />
                    </Row>
                  </div>
                </Form>
              </Layout>
            </Layout>
          </Layout>
        </Layout>
      </div>
    );
  }
}
const mapStateToProps = state => {
  console.log("mapStateToProps", state);
  return {
    UserInfo: state.UserInfo
  };
};
const mapDispatchToProps = dispatch => {
  console.log("dispatch ", dispatch);
  return {
    EditProfile: data =>
      dispatch({
        type: "EDIT_USER",
        payload: data
      })
  };
};
export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Profile)
);
