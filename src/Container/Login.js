import React, { Component } from "react";
import {
  Form,
  Icon,
  Input,
  Button,
  Divider,
  message,
  Col,
  Row,
  Carousel
} from "antd";
import { connect } from "react-redux";
import axios from "axios";
import { auth, provider } from "../firebase";
const image = ({} = "https://images.unsplash.com/photo-1441850605338-1b0b5a22e7b9?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80");
class Login extends Component {
  state = {
    isLoading: false,
    email: "",
    password: "",
    isLogin: false,
    imageUrl: ""
  };

  onEmailChange = event => {
    const email = event.target.value;

    this.setState({ email });
  };
  onPasswordChange = event => {
    const password = event.target.value;
    this.setState({ password });
  };

  onClickLoginWithFacebook = () => {
    this.setState({ isLoading: true });
    auth.signInWithPopup(provider).then(({ user }) => {
      console.log("user facebook ====>", user);
      axios
        .post("http://3.88.222.254:8888/customer/register", {
          email: user.email,
          password: 123456,
          image: user.photoURL
        })
        .then(res => {
          this.setState({ email:res.data.email,password:res.data.password})
          console.log("res ================> facebook", res.data.email);
          console.log("res ================> facebook", res.data.password);
          console.log("res ================> facebook", res.data.image);
          
            
        }).then(()=>{
          this.onClickLogin()
        })
        
        .catch(err => {
          message.error(err.message, 3);
        });
      // console.log("facebook ======================>", user);
      // this.setState({ imageUrl: `${user.photoURL}?height=500` });
      // this.navigateToMainPage();
    });
  };

  navigateToMainPage = () => {
    const { history } = this.props;
    history.push("/home");
  };
  onClickLogin = () => {
    console.log("this", this.state.email);
    axios({
      url: "http://3.88.222.254:8888/auth",
      method: "post",
      data: {
        username: this.state.email,
        password: this.state.password
      }
    })
      .then(data => {
        console.log(data.data);

        console.log("token", data);
        this.setState({ isLogin: true });
        this.props.user(data.data);
        this.navigateToMainPage();
      })
      .catch(error => {
        console.log("error" + error);
      });
  };

  render() {
    return (
      <div
        style={{
          paddingTop: "60px",
          // backgroundImage: `url(${image})`,
          flex: 1
        }}
      >
        <h1 style={{ paddingBottom: "40px" }}>Login CMU GUIDE</h1>{" "}
        <div
          style={{
            marginTop: "150px",
            backgroundColor: "#F4F6F6",
            // paddingRight:"250px",
            // paddingLeft:"250px",
            alignContent: "center",
            justifyContent: "center",
            margin: "0 auto",
            // margin: auto;
            width: "50%",
            // marginLeft: "auto",
            // marginRight: "auto",
            border: "3px solid #F8F9F9",
            padding: "10px"
          }}
        >
          <Row>
            <Col span={8}>
              <div style={{ marginLeft: "auto", marginRight: "auto" }}>
                <Carousel>
                  <img
                    src="https://images.unsplash.com/photo-1511690656952-34342bb7c2f2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=700&q=80"
                    width="100%"
                    height="300px"
                  />
                  <img
                    src="https://images.unsplash.com/photo-1533959938354-70634c231c21?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=634&q=80"
                    width="100%"
                    height="300px"
                  />
                </Carousel>
              </div>
            </Col>

            <Col span={16}>
              <div style={{ paddingTop: "25px" }}>
                <Form className="login-form">
                  <Form.Item>
                    <Input
                      prefix={
                        <Icon
                          type="user"
                          style={{ color: "rgba(0,0,0,.25)" }}
                        />
                      }
                      placeholder="Username"
                      value={this.state.email}
                      onChange={this.onEmailChange}
                    />
                  </Form.Item>
                  <Form.Item>
                    <Input
                      prefix={
                        <Icon
                          type="lock"
                          style={{ color: "rgba(0,0,0,.25)" }}
                        />
                      }
                      type="password"
                      placeholder="Password"
                      value={this.state.password}
                      onChange={this.onPasswordChange}
                    />
                  </Form.Item>
                  <div style={{ textAlign: "end" }}>
                    Or{" "}
                    <a
                      onClick={() => {
                        this.props.history.push("/register");
                      }}
                    >
                      register now !
                    </a>
                  </div>

                  <Divider />
                  <Form.Item>
                    <Row>
                      <Col span={8}>
                        <Button
                          type="primary"
                          htmlType="submit"
                          className="login-form-button"
                          loading={this.state.isLogin}
                          onClick={() => {
                            this.onClickLogin();
                          }}
                        >
                          Log in
                        </Button>
                      </Col>
                      <Col span={12}>
                        <Button
                          type="primary"
                          icon="facebook"
                          style={{ marginLeft: "12px" }}
                          loading={this.state.isLoading}
                          onClick={this.onClickLoginWithFacebook}
                        >
                          Login With Facebook
                        </Button>
                      </Col>
                    </Row>
                  </Form.Item>
                </Form>
              </div>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}
const mapDispatchToProps = dispatch => {
  console.log("dispatch ", dispatch);
  return {
    user: data =>
      dispatch({
        type: "ADD_USER",
        addData: data
      })
  };
};
export default connect(
  null,
  mapDispatchToProps
)(Login);
