import React, { Component } from "react";
import {
  Button,
  Avatar,
  Typography,
  Row,
  Col,
  Form,
  Divider,
  Upload,
  message,
  Icon,
  Layout,
  Carousel,
  Input
} from "antd";
import { connect } from "react-redux";
import axios from "axios";
import { withRouter } from "react-router-dom";
import SiderProfile from "../Component/Profile/SiderProfile";
const { Title, Paragraph, Text } = Typography;
class Edit extends Component {
  state = {
    firstName: "",
    lastName: "",
    email: "",
    isLoading: false
  };
  onLastNameChange = event => {
    const lastName = event.target.value;

    this.setState({ lastName });
  };
  onFirstNameChange = event => {
    const firstName = event.target.value;

    this.setState({ firstName });
  };
  onClickSave = () => {
    const { email, firstName, lastName } = this.state;
    const { id } = this.props.UserInfo.user;
    const { token } = this.props.UserInfo;
    // console.log(
    //   "this.props.UserInfo.user.id ===============> Edit ",
    //   this.props.UserInfo
    // );

    axios({
      url: `http://3.88.222.254:8888/edit/profile/${id}`,
      method: "put",
      headers: { Authorization: `Bearer ${token}` },
      data: {
        firstName,
        lastName
      }
    })
      .then(res => {
       
        console.log("Res edit page ===================>", res.data);
        // this.props.location.push("/profile")
        this.props.EditProfile(res.data) 
        message.success("Edit Sucessfuly",1,()=>{this.props.history.push("/profile")} );
      })
      .catch(err => {
        this.setState({ isLoading: false });
        message.error(err.message, 3);
      });
  };
  render() {
    const { firstName, lastName, email } = this.props.UserInfo.user;
    const {image}=this.props.UserInfo
    console.log("email",this.props.UserInfo)
    return (
      <div>
        <Layout>
          <div>
            <Carousel width="100%" height="300px" autoplay>
              <img
                src="https://images.unsplash.com/photo-1504674900247-0877df9cc836?ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80"
                width="100%"
                height="300px"
              />
              <img
                src="https://images.unsplash.com/photo-1494114685051-b9989320801f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1043&q=80"
                width="100%"
                height="300px"
              />
              <img
                src="https://images.unsplash.com/photo-1528181304800-259b08848526?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80"
                width="100%"
                height="300px"
              />
            </Carousel>
          </div>

          <Layout>
            <SiderProfile />
            <Layout>
              <div
                style={{
                  paddingTop: "100px"
                }}
              >
                <Avatar
                  size={200}
                  src={image}
                  style={{ color: "#f56a00", backgroundColor: "#fde3cf" }}
                  icon="user"
                />
              </div>
              <div style={{ paddingTop: "50px" }}>
                <Upload>
                  <Button>
                    <Icon type="upload" /> Upload Image
                  </Button>
                </Upload>
              </div>

              <Layout style={{ padding: "0 24px 24px" }}>
                <Form style={{ width: "50%", margin: "0 auto" }}>
                  <div
                    className="gutter-example"
                    style={{ paddingTop: "100px" }}
                  >
                    <Row gutter={16}>
                      <Col className="gutter-row" span={8}>
                        <div className="gutter-box">
                          {" "}
                          <Title level={4}>ชื่อ :</Title>
                        </div>
                      </Col>
                      <Col className="gutter-row" span={8}>
                        <Input placeholder="ชื่อ"
                          value={this.state.firstName}
                          onChange={this.onFirstNameChange} />
                      </Col>
                      <Divider />
                    </Row>
                    <Row>
                      <Col className="gutter-row" span={8}>
                        <div className="gutter-box">
                          <Title level={4}>นามสกุล :</Title>
                        </div>
                      </Col>

                      <Col className="gutter-row" span={8}>
                        <Input
                          placeholder="นามสกุล"
                          value={this.state.lastName}
                          onChange={this.onLastNameChange}
                        />
                        
                      </Col>
                      <Divider />
                    </Row>
                    <Row gutter={16}>
                      <Col className="gutter-row" span={8}>
                        <div className="gutter-box">
                          {" "}
                          <Title level={4}>อีเมล์ : </Title>
                        </div>
                      </Col>
                      <Col className="gutter-row" span={4}>
                        <div className="gutter-box">{email}</div>
                      </Col>
                      <Divider />
                    </Row>
                    <Row gutter={16}>
                      <Button
                        style={{ width: "44%" }}
                        htmlType="submit"
                        type="primary"
                        loading={this.state.isLoading}
                        onClick={() => {
                          this.onClickSave();
                        }}
                      >
                        Save
                      </Button>
                    </Row>
                  </div>
                </Form>
              </Layout>
            </Layout>
          </Layout>
        </Layout>
      </div>
    );
  }
}
const mapStateToProps = state => {
  console.log("mapStateToProps", state);
  return {
    UserInfo: state.UserInfo
  };
};

const mapDispatchToProps = dispatch => {
  console.log("dispatch ", dispatch);
  return {
    EditProfile: data =>
      dispatch({
        type: "EDIT_USER",
        payload: data
      })
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Edit)
);
