import React, { Component } from "react";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import Shop from "./Shop";
import Maps from "../Container/MapPage";
import ShopList from "../Component/ShopList";
import Register from "../Container/Register";
import MapByShop from "../Component/Shop/Map";

class RouteHeader extends Component {
  render() {
    return (
      <div style={{ width: "100%" }}>
        <Switch>
          <Route excat path="/map" component={Maps} />

          <Route exact path="/home" component={ShopList} />
          {/* <Route exact path="/register" component={Register} /> */}
          <Route exact path="/shop" component={Shop} />
          <Route exact path="/mapbyshop" component={MapByShop} />
          <Route exact path="/" component={ShopList} />
        </Switch>
      </div>
    );
  }
}
export default RouteHeader;
