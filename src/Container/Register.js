import React, { Component } from "react";
import {
  Form,
  Input,
  Divider,
  Icon,
  Col,
  Row,
  message,
  Button,
  Carousel
} from "antd";
import { auth } from "../firebase";
import axios from "axios";

class Register extends Component {
  state = {
    email: "",
    password: "",
    isLoading: false
  };
  onEmailChange = event => {
    const email = event.target.value;

    this.setState({ email });
  };
  onPasswordChange = event => {
    const password = event.target.value;
    this.setState({ password });
  };
  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  onSubmitFormRegister = e => {
    e.preventDefault();
    this.setState({ isLoading: true });
    const email = this.state.email;
    const password = this.state.password;
    const isEailValid = this.validateEmail(email);
    if (isEailValid) {
      axios
        .post("http://3.88.222.254:8888/customer/register", { email, password })

        .then(res => {
          console.log("res register ==================>", res);
          this.setState({ isLoading: false });
          message.success("Successfully", 2, () => {
            this.props.history.push("/login");
          });
        })
        .catch(err => {
          this.setState({ isLoading: false });
          message.error(err.message, 3);
        });
    } else {
      this.setState({ isLoading: false });
      message.error("Email invalid!", 1);
    }
  };
  render() {
    return (
      <div
        style={{
          paddingTop: "60px",
          // backgroundImage: `url(${image})`,
          flex: 1
        }}
      >
        <h1 style={{ paddingBottom: "40px" }}>Register CMU GUIDE</h1>{" "}
        <div
          style={{
            marginTop: "150px",
            backgroundColor: "#F4F6F6",
            // paddingRight:"250px",
            // paddingLeft:"250px",
            alignContent: "center",
            justifyContent: "center",
            margin: "0 auto",
            // margin: auto;
            width: "50%",
            // marginLeft: "auto",
            // marginRight: "auto",
            border: "3px solid #F8F9F9",
            padding: "10px"
          }}
        >
          <Row>
            <Col span={8}>
              <div style={{ marginLeft: "auto", marginRight: "auto" }}>
                <Carousel>
                  <img
                    src="https://images.unsplash.com/photo-1511690656952-34342bb7c2f2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=700&q=80"
                    width="100%"
                    height="300px"
                  />
                  <img
                    src="https://images.unsplash.com/photo-1533959938354-70634c231c21?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=634&q=80"
                    width="100%"
                    height="300px"
                  />
                </Carousel>
              </div>
            </Col>

            <Col span={16}>
              <div style={{ paddingTop: "25px" }}>
                <Form
                  className="login-form"
                  onSubmit={this.onSubmitFormRegister}
                >
                  <Form.Item>
                    <Input
                      prefix={
                        <Icon
                          type="user"
                          style={{ color: "rgba(0,0,0,.25)" }}
                        />
                      }
                      placeholder="Email"
                      value={this.state.email}
                      onChange={this.onEmailChange}
                    />
                  </Form.Item>
                  <Form.Item>
                    <Input
                      prefix={
                        <Icon
                          type="lock"
                          style={{ color: "rgba(0,0,0,.25)" }}
                        />
                      }
                      type="password"
                      placeholder="Password"
                      value={this.state.password}
                      onChange={this.onPasswordChange}
                    />
                  </Form.Item>
                  <div style={{ textAlign: "end" }}>
                    Or{" "}
                    <a
                      onClick={() => {
                        this.props.history.push("/register");
                      }}
                    >
                      register now !
                    </a>
                  </div>

                  <Divider />
                  <Form.Item>
                    <Form.Item>
                      <Button
                        style={{ width: "44%" }}
                        htmlType="submit"
                        type="primary"
                        loading={this.state.isLoading}
                      >
                        Register
                      </Button>
                    </Form.Item>
                  </Form.Item>
                </Form>
              </div>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}
export default Register;
