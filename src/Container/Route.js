import React, { Component } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Login from "./Login";
import Profile from "./Profile";
import Register from "./Register";
import { store, persistor } from "../Stroe/store";
import { Provider } from "react-redux";
import Main from "./Main";
import { PersistGate } from "redux-persist/integration/react";
import Edit from './Edit'
class Routes extends Component {
  render() {
    return (
      <div style={{ width: "100%" }}>
        <Provider store={store}>
          <PersistGate loading={null} persistor={persistor}>
            <BrowserRouter>
              <Switch>
                <Route exact path="/Login" component={Login} />
                <Route exact path="/" component={Main} />
                <Route exact path="/register" component={Register} />
                <Route exact path="/profile" component={Profile} />
                <Route exact path="/edit" component={Edit}/>
                <Route component={Main} />
              </Switch>
            </BrowserRouter>
          </PersistGate>
        </Provider>
      </div>
    );
  }
}
export default Routes;
